import React from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import {Layout} from 'antd';
import Navbar from './components/Navbar';
import Footer from './components/Footer';
import Home from './components/Home';
import Login from "./components/Login";
import Register from "./components/Register";
import UserProfile from "./components/UserProfile";
import UserProvider from "./contexts/UserProvider";

const { Content } = Layout;

function App() {
  return (
      <UserProvider>
          <Router>
              <Layout className="App">
                  <Navbar />
                  <Content style={{ padding: '0 50px' }}>
                      <Routes>
                          <Route path="/" exact element={<Home />} />
                          <Route path="/login" element={<Login/>} />
                          <Route path="/register" element={<Register />} />
                          <Route path="/profile" element={<UserProfile />} />
                      </Routes>
                  </Content>
                  <Footer />
              </Layout>
          </Router>
      </UserProvider>
  );
}

export default App;
