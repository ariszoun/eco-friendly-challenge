import React, {useContext, useEffect, useState} from 'react';
import axios from 'axios';
import UserContext from "../contexts/UserContext";

const UserProfile = () => {
    const [userData, setUserData] = useState({});
    const { user , setUser} = useContext(UserContext);


    useEffect(() => {
        (async () => {
            try {
                // Get user token from local storage
                const userToken = localStorage.getItem('token');

                // Fetch user data using the token
                const config = {
                    headers: {
                        'Authorization': `Bearer ${userToken}`
                    }
                };

                const response = await axios.get('http://localhost:8080/api/v1/users/email' + '?email=' + user?.email, config);
                setUserData(response.data);
                setUser(response.data);
            } catch (error) {
                console.error("Error fetching user data:", error);
            }
        })();
    }, [user?.email, setUser]);

    return (
        <div>
            <h2>Welcome, {userData.username}</h2>
            <p>Email: {userData.email}</p>
        </div>
    );
}

export default UserProfile;