import React, {useContext, useState} from 'react';
import { Form, Input, Button, message } from 'antd';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import UserContext from "../contexts/UserContext";

const Login = () => {
    const [formData, setFormData] = useState({
        email: '',
        password: ''
    });

    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        });
    };

    const navigateToProfile = useNavigate();
    const { setUser } = useContext(UserContext);
    const handleSubmit = async () => {

        try {
            const response = await axios.post('http://localhost:8080/api/v1/auth/login', formData);
            // Store the JWT token to localStorage or in a cookie
            localStorage.setItem('token', response.data.token);
            message.success("Logged in successfully!");

            setUser({
                email: formData.email,
            })

            // Redirect to user profile
            navigateToProfile('/profile');
        } catch (error) {
            message.error("Login failed!");
        }
    };

    return (
        <div className="login-container">
            <Form onFinish={handleSubmit} className="login-form">
                <Form.Item label="Email">
                    <Input name="email" type="email" value={formData.email} onChange={handleChange} />
                </Form.Item>
                <Form.Item label="Password">
                    <Input name="password" type="password" value={formData.password} onChange={handleChange} />
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit">Login</Button>
                </Form.Item>
            </Form>
        </div>
    );
}

export default Login;