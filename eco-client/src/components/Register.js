import React, { useState } from 'react';
import { Form, Input, Button, message } from 'antd';
import axios from 'axios';

const Register = () => {
    const [formData, setFormData] = useState({
        username: '',
        email: '',
        password: ''
    });

    const handleChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name]: e.target.value
        });
    };

    const handleSubmit = async () => {
        try {
            const response = await axios.post('http://localhost:8080/api/v1/auth/register', formData);
            message.success("Registered successfully!");
            // On Success redirect user to login page or handle as needed
        } catch (error) {
            message.error("Registration failed!");
        }
    };

    return (
        <div className="register-container">
            <Form onFinish={handleSubmit} className="register-form">
                <Form.Item label="Username">
                    <Input name="username" value={formData.username} onChange={handleChange} />
                </Form.Item>
                <Form.Item label="Email">
                    <Input name="email" type="email" value={formData.email} onChange={handleChange} />
                </Form.Item>
                <Form.Item label="Password">
                    <Input name="password" type="password" value={formData.password} onChange={handleChange} />
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit">Register</Button>
                </Form.Item>
            </Form>
        </div>
    );
}

export default Register;