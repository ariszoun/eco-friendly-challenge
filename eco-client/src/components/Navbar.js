import React, {useContext} from 'react';
import {Menu, message} from 'antd';
import { Link } from 'react-router-dom';
import UserContext from "../contexts/UserContext";
import { useNavigate } from 'react-router-dom';
import axios from "axios";

const Navbar = () => {
    const { user, setUser } = useContext(UserContext);
    const navigateToHomePage = useNavigate();

    const token = localStorage.getItem('token');

    const handleLogout = async () => {
        // Call to backend to logout (even if it does minimal work, it's good for consistency)
        try {
            await axios.post('http://localhost:8080/api/v1/auth/logout', null, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });
            // Remove the JWT from local storage
            localStorage.removeItem('token');
            // Set user to null to update the Navbar
            setUser(null);
            navigateToHomePage('/');
            message.info("You logged out");
        } catch (error) {
            console.error("Error during logout:", error);
        }
    };

    return (
        <div className="navbar">
            <Menu mode="horizontal">
                <Menu.Item key="home">
                    <Link to="/">Home</Link>
                </Menu.Item>
                {/* Display Challenges and Prizes only if a user is logged in */}
                {user && (
                    <>
                        <Menu.Item key="challenges">
                            <Link to="/challenges">Challenges</Link>
                        </Menu.Item>
                        <Menu.Item key="prizes">
                            <Link to="/prizes">Prizes</Link>
                        </Menu.Item>
                    </>
                )}
                <Menu.Item key="profile">
                    {user ?
                        <span>Logged in as {user.username}</span> :
                        <Link to="/login">Login</Link>}
                </Menu.Item>
                {/* Display Register only if a user is NOT logged in */}
                {!user && (
                    <Menu.Item key="register">
                        <Link to="/register">Register</Link>
                    </Menu.Item>
                )}
                {user && (
                    <Menu.Item key="logout" onClick={handleLogout}>
                        Logout
                    </Menu.Item>
                )}
                {/*<Menu.Item key="event-dashboard">*/}
                {/*    <Link to="/events-dashboard">Event Dashboard</Link>*/}
                {/*</Menu.Item>*/}
            </Menu>
        </div>
    );
}

export default Navbar;