import React from 'react';
import { Layout } from 'antd';

const { Footer: AntFooter } = Layout;

const Footer = () => {
    return (
        <AntFooter className="footer" style={{ textAlign: 'center' }}>
            Eco Challenge ©2023 Created by Aris
        </AntFooter>
    );
}

export default Footer;