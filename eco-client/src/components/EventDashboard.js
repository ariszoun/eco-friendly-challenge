// import React, { useState, useEffect } from 'react';
// import axios from 'axios';
//
// const EventDashboard = () => {
//     const [messages, setMessages] = useState([]);
//
//     useEffect(() => {
//         // Fetch messages from the backend
//         (async () => {
//             try {
//                 const response = await axios.get('http://localhost:8080/api/v1/messages');
//                 setMessages(response.data);
//             } catch (error) {
//                 console.error('Error in fetchMessages:', error);
//             }
//         })();
//     }, []);
//
//     return (
//         <div>
//             <h2>Event Dashboard</h2>
//             <ul>
//                 {messages.map((message, index) => (
//                     <li key={index}>{message}</li>
//                 ))}
//             </ul>
//         </div>
//     );
// };
//
// export default EventDashboard;
