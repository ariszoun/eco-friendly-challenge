import React from 'react';
import {Link} from "react-router-dom";
import {Button} from "antd";

const Home = () => {
    return (
        <div className="home-container">
            <div className="home-content">
                <h1>Welcome to Eco Challenge</h1>
                <div className="home-buttons">
                    <Link to="/login">
                        <Button
                            type="primary"
                            size="large"
                            style={{
                                marginRight: '10px',
                                backgroundColor: '#508250',
                                borderColor: '#a4de74'
                            }}
                            hoverable={true}
                        >
                            Login
                        </Button>
                    </Link>
                    <Link to="/register">
                        <Button
                            type="primary"
                            size="large"
                            style={{
                                backgroundColor: '#8cc152',
                                borderColor: '#508250'
                            }}
                            hoverable={true}
                        >
                            Register
                        </Button>
                    </Link>
                </div>
            </div>
        </div>
    );
}

export default Home;