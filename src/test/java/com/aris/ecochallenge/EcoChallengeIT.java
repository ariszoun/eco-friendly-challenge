package com.aris.ecochallenge;

import com.aris.ecochallenge.model.*;
import com.aris.ecochallenge.repository.RoleRepository;
import com.aris.ecochallenge.repository.UserChallengeRepository;
import com.aris.ecochallenge.service.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

// TODO check test security context and roles
@SpringBootTest(properties = "spring.profiles.active=test")
@Transactional
@WithMockUser(username="testUser", roles={"ADMIN"})
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class EcoChallengeIT {

    @Autowired
    private EntityManager entityManager;

    @Autowired
    private UserService userService;

    @Autowired
    private ChallengeService challengeService;

    @Autowired
    private UserChallengeService userChallengeService;

    @Autowired
    private PrizeService prizeService;

    @Autowired
    private UserPrizeService userPrizeService;

    @Autowired
    private RoleRepository roleRepository;

    @BeforeEach
    public void setup() {
    }

    @Test
    public void testEcoChallengeFlow() {

        // Create and persist a user
        User user = new User();
        user.setUsername("testUsername");
        user.setEmail("test@example.com");
        user.setPassword("password123");
        user.setRoles(Set.of(Objects.requireNonNull(roleRepository.findByName("ROLE_USER").orElse(null))));
        User persistedUser = userService.registerUser(user);
        assertNotNull(persistedUser.getId());
        assertEquals(persistedUser.getRoles().size(), 1);
        persistedUser.getRoles().stream().findFirst().ifPresent(role -> assertEquals(role.getName(), "ROLE_USER"));

        // Create and persist challenges
        Challenge ch1 = new Challenge("Title 1", "Description 1", 2);
        Challenge ch2 = new Challenge("Title 2", "Description 2", 3);
        challengeService.createChallenge(ch1);
        challengeService.createChallenge(ch2);
        assertNotNull(challengeService.getChallengeById(ch1.getId()));
        assertNotNull(challengeService.getChallengeById(ch2.getId()));
        List<Challenge> allChallenges = challengeService.listAllChallenges();
        assertEquals(allChallenges.size(), 2);

        //Create and persist prizes
        Prize prize = new Prize("Prize 1", "Description 1", 5);
        Prize prize2 = new Prize("Prize 2", "Description 2", 5);
        prizeService.createPrize(prize);
        prizeService.createPrize(prize2);
        assertEquals(prizeService.getAllPrizes().size(), 2);

        // Assign a challenge to a user
        userChallengeService.assignChallengeToUser(persistedUser.getId(), ch1.getId());
        userChallengeService.assignChallengeToUser(persistedUser.getId(), ch2.getId());
        assertSame(userChallengeService.getStatusForUserChallenge(persistedUser.getId(), ch1.getId()), Status.IN_PROGRESS);
        assertSame(userChallengeService.getStatusForUserChallenge(persistedUser.getId(), ch2.getId()), Status.IN_PROGRESS);

        // Complete challenge and check assigned points
        userChallengeService.completeChallenge(persistedUser.getId(), ch1.getId());
        assertSame(userChallengeService.getStatusForUserChallenge(persistedUser.getId(), ch1.getId()), Status.COMPLETED);
        assertEquals(persistedUser.getTotalPoints(), 2);
        userChallengeService.completeChallenge(persistedUser.getId(), ch2.getId());
        assertSame(userChallengeService.getStatusForUserChallenge(persistedUser.getId(), ch2.getId()), Status.COMPLETED);
        assertEquals(persistedUser.getTotalPoints(), 5);
        assertEquals(persistedUser.getUserChallenges().size(), 0);

        // Assign Prize to user if the user is eligible and reset its points
        userPrizeService.assignPrizeIfUserIsEligible(persistedUser.getId());
        assertEquals(userPrizeService.getAllUserPrizes().size(), 1);
        assertEquals(persistedUser.getTotalPoints(), 0);

        //delete entities
        userPrizeService.deleteAllPrizesFromUser(persistedUser.getId());
        assertEquals(userPrizeService.getAllUserPrizes().size(), 0);
        userChallengeService.unassignChallengeFromUser(persistedUser.getId(), ch1.getId());
        userChallengeService.unassignChallengeFromUser(persistedUser.getId(), ch2.getId());
        assertEquals(persistedUser.getUserChallenges().size(), 0);
        challengeService.deleteChallenge(ch1.getId());
        challengeService.deleteChallenge(ch2.getId());
        assertEquals(challengeService.listAllChallenges().size(), 0);
        userService.deleteUser(persistedUser.getId());
        assertFalse(userService.getUserById(persistedUser.getId()).isPresent());
    }

    @AfterEach
    public void tearDown() {
        entityManager.clear();
    }
}
