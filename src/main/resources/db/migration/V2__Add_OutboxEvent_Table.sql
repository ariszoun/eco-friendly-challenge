CREATE TABLE outbox_event (
                              id SERIAL PRIMARY KEY,
                              event_type VARCHAR(255) NOT NULL,
                              payload TEXT,
                              created_date TIMESTAMP WITHOUT TIME ZONE DEFAULT CURRENT_TIMESTAMP
);
