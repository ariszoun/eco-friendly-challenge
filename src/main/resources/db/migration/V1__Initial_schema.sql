-- User Table
CREATE TABLE users (
                       id SERIAL PRIMARY KEY,
                       username VARCHAR(255) NOT NULL UNIQUE,
                       password VARCHAR(255) NOT NULL,
                       email VARCHAR(255) NOT NULL UNIQUE,
                       total_points INTEGER DEFAULT 0
);

-- Challenge Table
CREATE TABLE challenges (
                            id SERIAL PRIMARY KEY,
                            title VARCHAR(255) NOT NULL,
                            description TEXT NOT NULL,
                            point_value INTEGER NOT NULL
);

-- UserChallenge Table (Join table for Users and Challenges)
CREATE TABLE user_challenges (
                                 id SERIAL PRIMARY KEY,
                                 user_id INTEGER REFERENCES users(id),
                                 challenge_id INTEGER REFERENCES challenges(id),
                                 status VARCHAR(50) DEFAULT 'in-progress',
                                 start_date DATE DEFAULT CURRENT_DATE,
                                 completion_date DATE
);

-- Prize Table
CREATE TABLE prizes (
                        id SERIAL PRIMARY KEY,
                        title VARCHAR(255) NOT NULL,
                        description TEXT NOT NULL,
                        point_threshold INTEGER NOT NULL
);

-- UserPrize Table (Join table for Users and Prizes)
CREATE TABLE user_prizes (
                             id SERIAL PRIMARY KEY,
                             user_id INTEGER REFERENCES users(id),
                             prize_id INTEGER REFERENCES prizes(id),
                             claim_date DATE DEFAULT CURRENT_DATE
);

-- Adding some indices for better query performance
CREATE INDEX idx_user_username ON users(username);
CREATE INDEX idx_user_email ON users(email);
CREATE INDEX idx_challenge_title ON challenges(title);
CREATE INDEX idx_prize_title ON prizes(title);
