package com.aris.ecochallenge.exception;

public class EntityNotFoundException extends RuntimeException{
    public EntityNotFoundException(String entity, String identifier) {
        super(entity + " not found with identifier: " + identifier);
    }
}
