package com.aris.ecochallenge.exception;

public class EntityAlreadyExistsException extends RuntimeException {
    public EntityAlreadyExistsException(String entity, String identifier) {
        super(entity + " already exists with identifier: " + identifier);
    }
}
