package com.aris.ecochallenge.repository;

import com.aris.ecochallenge.model.Challenge;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ChallengeRepository extends JpaRepository<Challenge, Long> {

    // Find challenge by title. (helps for duplicates)
    Optional<Challenge> findChallengeByTitle(String title);
}
