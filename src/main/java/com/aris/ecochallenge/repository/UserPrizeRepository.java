package com.aris.ecochallenge.repository;

import com.aris.ecochallenge.model.User;
import com.aris.ecochallenge.model.UserPrize;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserPrizeRepository extends JpaRepository<UserPrize, Long> {
    // Find all prizes claimed by a specific user.
    List<UserPrize> findByUserId(Long userId);
    List<UserPrize> findAllByUser(User user);
}
