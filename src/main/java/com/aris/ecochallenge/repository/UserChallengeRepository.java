package com.aris.ecochallenge.repository;

import com.aris.ecochallenge.model.Challenge;
import com.aris.ecochallenge.model.Status;
import com.aris.ecochallenge.model.User;
import com.aris.ecochallenge.model.UserChallenge;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserChallengeRepository extends JpaRepository<UserChallenge, Long> {
    // Find challenges taken up by a specific user.
    List<UserChallenge> findByUserId(Long userId);

    // Find specific challenge status for a user.
    Optional<UserChallenge> findByUserIdAndChallengeId(Long userId, Long challengeId);

    UserChallenge findByUserAndChallenge(User user, Challenge challenge);

    List<UserChallenge> findByUserAndStatus(User user, Status status);
}
