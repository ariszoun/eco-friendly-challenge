package com.aris.ecochallenge.repository;

import com.aris.ecochallenge.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    //Find user by email (useful for login).
    Optional<User> findByEmail(String email);
}
