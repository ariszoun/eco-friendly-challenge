package com.aris.ecochallenge.repository;

import com.aris.ecochallenge.model.Prize;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PrizeRepository extends JpaRepository<Prize, Long> {
    // Find all prizes below a certain point threshold
    List<Prize> findPrizesByPointThresholdLessThanEqual(int pointThreshold);

    /**
     * Finds a prize by its point threshold.
     *
     * @param threshold The point threshold to search for.
     * @return An Optional containing the found prize, or an empty Optional if no prize was found.
     */
    Optional<Prize> findPrizeByPointThreshold(int threshold);
}
