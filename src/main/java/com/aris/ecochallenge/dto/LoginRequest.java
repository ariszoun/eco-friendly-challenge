package com.aris.ecochallenge.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import org.springframework.stereotype.Component;


/**
 * Dummy object to only transfer data between layers & processes.
 * When a LoginRequest hits controller ep, controller will receive the request in the shape of this DTO.
 * */
public class LoginRequest {
    @Email
    @NotNull
    private String email;

    @NotNull
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
