//package com.aris.ecochallenge.service;
//
//import org.springframework.kafka.annotation.KafkaListener;
//import org.springframework.stereotype.Service;
//
//import java.util.ArrayList;
//import java.util.List;
//
//@Service
//public class KafkaConsumerService {
//    private final List<String> messages = new ArrayList<>();
//
//    @KafkaListener(topics = "eco-friendly-challenge-db.public.outbox_event", groupId = "admin-dashboard-updates-group")
//    public void consume(String message) {
//        System.out.println("Received message: " + message);  // Log the message
//        synchronized (messages) {
//            messages.add(message);
//        }
//    }
//
//    public List<String> getMessages() {
//        synchronized (messages) {
//            List<String> currentMessages = new ArrayList<>(messages);
//            messages.clear();
//            return currentMessages;
//        }
//    }
//}
