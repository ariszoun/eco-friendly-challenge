//package com.aris.ecochallenge.service;
//
//import com.aris.ecochallenge.model.OutboxEvent;
//import com.aris.ecochallenge.repository.OutboxEventRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.kafka.core.KafkaTemplate;
//import org.springframework.stereotype.Service;
//
//@Service
//public class OutboxEventService {
//    @Autowired
//    private OutboxEventRepository outboxEventRepository;
//
//    @Autowired
//    private KafkaTemplate<String, String> kafkaTemplate;
//
//    public void processAndSendEvent(OutboxEvent event) {
//        // 1. Save the event in the database with status PENDING.
//        outboxEventRepository.save(event);
//
//        // 2. Send the event to Kafka.
//        try {
//            kafkaTemplate.send("your-topic-name", event.getPayload()); // You can customize this based on how you're sending messages.
//
//            // 3. Mark the event as SENT in the database.
//            event.setStatus(OutboxEvent.EventStatus.SENT);
//            outboxEventRepository.save(event);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//}
