package com.aris.ecochallenge.service;

import com.aris.ecochallenge.exception.EntityAlreadyExistsException;
import com.aris.ecochallenge.exception.EntityNotFoundException;
import com.aris.ecochallenge.model.Challenge;
import com.aris.ecochallenge.model.Status;
import com.aris.ecochallenge.model.User;
import com.aris.ecochallenge.model.UserChallenge;
import com.aris.ecochallenge.repository.ChallengeRepository;
import com.aris.ecochallenge.repository.UserChallengeRepository;
import com.aris.ecochallenge.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ChallengeService {

    private final ChallengeRepository challengeRepository;
    private final UserChallengeRepository userChallengeRepository;
    private final UserRepository userRepository;

    @Autowired
    public ChallengeService(ChallengeRepository challengeRepository, UserRepository userRepository,
                            UserChallengeRepository userChallengeRepository) {
        this.challengeRepository = challengeRepository;
        this.userRepository = userRepository;
        this.userChallengeRepository = userChallengeRepository;
    }

    /**
     * Create a new challenge but not a duplicate challenge
     * */
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Challenge createChallenge(Challenge challenge) {
        // Assume that no 2 challenges can have the same title
        if(challengeRepository.findChallengeByTitle(challenge.getTitle()).isPresent()) {
            throw new EntityAlreadyExistsException(Challenge.class.getName(), challenge.getTitle());
        }
        return challengeRepository.save(challenge);
    }

    public List<Challenge> listAllChallenges() {
        return challengeRepository.findAll();
    }

    public Challenge getChallengeById(Long challengeId) {
        return challengeRepository.findById(challengeId)
                .orElseThrow(() -> new EntityNotFoundException(Challenge.class.getName(),
                        String.valueOf(challengeId)));
    }


    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public UserChallenge assignChallengeToUser(Long userId, Long challengeId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new EntityNotFoundException(User.class.getName(), String.valueOf(userId)));
        Challenge challenge = challengeRepository.findById(challengeId)
                .orElseThrow(() -> new EntityNotFoundException(Challenge.class.getName(), String.valueOf(challengeId)));

        if(userChallengeRepository.findByUserAndChallenge(user, challenge) != null) {
            throw new EntityAlreadyExistsException(UserChallenge.class.getName(),
                    "User Id: " + userId + ", Challenge Id: " + challengeId);
        }

        UserChallenge userChallenge = new UserChallenge();
        userChallenge.setUser(user);
        userChallenge.setChallenge(challenge);
        userChallenge.setStatus(Status.IN_PROGRESS); // Assuming you have an ENUM for status

        return userChallengeRepository.save(userChallenge);
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public Challenge updateChallenge(Challenge challenge) {
        if(!challengeRepository.existsById(challenge.getId())) {
            throw new EntityNotFoundException(Challenge.class.getName(),
                    String.valueOf(challenge.getId()));
        }
        return challengeRepository.save(challenge);
    }

    //TODO fix role and checks
    @PreAuthorize("hasRole('ROLE_USER') or hasRole('ROLE_ADMIN')")
    public void deleteChallenge(Long challengeId) {
        if(!challengeRepository.existsById(challengeId)) {
            throw new EntityNotFoundException(Challenge.class.getName(), String.valueOf(challengeId));
        }
        challengeRepository.deleteById(challengeId);
    }

}
