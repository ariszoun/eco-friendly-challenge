package com.aris.ecochallenge.service;

import com.aris.ecochallenge.exception.EntityNotFoundException;
import com.aris.ecochallenge.model.*;
import com.aris.ecochallenge.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class UserChallengeService {

    private final UserChallengeRepository userChallengeRepository;
    private final ChallengeRepository challengeRepository;
    private final UserRepository userRepository;
    private final PrizeRepository prizeRepository;
    private final UserPrizeRepository userPrizeRepository;


    @Autowired
    public UserChallengeService(UserChallengeRepository userChallengeRepository,
                                ChallengeRepository challengeRepository,
                                UserRepository userRepository,
                                PrizeRepository prizeRepository,
                                UserPrizeRepository userPrizeRepository) {
        this.userChallengeRepository = userChallengeRepository;
        this.challengeRepository = challengeRepository;
        this.userRepository = userRepository;
        this.prizeRepository = prizeRepository;
        this.userPrizeRepository = userPrizeRepository;
    }

    public UserChallenge assignChallengeToUser(Long userId, Long challengeId) {
        // Fetch the user and challenge by their IDs
        User user = userRepository.findById(userId).orElseThrow(() -> new EntityNotFoundException(User.class.getName(), String.valueOf(userId)));
        Challenge challenge = challengeRepository.findById(challengeId).orElseThrow(() -> new EntityNotFoundException(Challenge.class.getName(), String.valueOf(challengeId)));

        // Check if challenge is already assigned
        UserChallenge existingUserChallenge = userChallengeRepository.findByUserAndChallenge(user, challenge);
        if (existingUserChallenge != null) {
            throw new IllegalArgumentException("Challenge already assigned to user");
        }

        UserChallenge userChallenge = new UserChallenge();
        userChallenge.setUser(user);
        userChallenge.setChallenge(challenge);
        userChallenge.setStatus(Status.IN_PROGRESS);

        return userChallengeRepository.save(userChallenge);
    }

    public void unassignChallengeFromUser(Long userId, Long challengeId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new EntityNotFoundException(User.class.getName(), String.valueOf(userId)));
        Challenge challenge = challengeRepository.findById(challengeId)
                .orElseThrow(() -> new EntityNotFoundException(Challenge.class.getName(), String.valueOf(challengeId)));

        UserChallenge existingUserChallenge = userChallengeRepository.findByUserAndChallenge(user, challenge);
        if (existingUserChallenge == null) {
            throw new IllegalArgumentException("Challenge is not assigned to user");
        }

        userChallengeRepository.delete(existingUserChallenge);
    }

//    public UserChallenge completeChallenge(Long userId, Long challengeId) {
//        User user = userRepository.findById(userId).orElseThrow(() -> new EntityNotFoundException(User.class.getName(), String.valueOf(userId)));
//        Challenge challenge = challengeRepository.findById(challengeId).orElseThrow(() -> new EntityNotFoundException(Challenge.class.getName(), String.valueOf(challengeId)));
//
//        UserChallenge existingUserChallenge = userChallengeRepository.findByUserAndChallenge(user, challenge);
//        if (existingUserChallenge == null) {
//            throw new IllegalArgumentException("Challenge is not assigned to user");
//        }
//
//        existingUserChallenge.setStatus(Status.COMPLETED);
//        existingUserChallenge.setCompletionDate(LocalDate.now());
//
//        // Add challenge points to user's total points
//        user.setTotalPoints(user.getTotalPoints() + challenge.getPointValue());
//
//        // Check for prize eligibility
//        if(user.getTotalPoints() >= 5) {
//            // Assign a prize to user
//            Prize prize = prizeRepository.findPrizeByPointThreshold(5)
//                    .orElseThrow(() -> new EntityNotFoundException(Prize.class.getName(), "Point threshold: 5"));
//
//            UserPrize userPrize = new UserPrize();
//            userPrize.setUser(user);
//            userPrize.setPrize(prize);
//            userPrizeRepository.save(userPrize);
//
//            // Reset user's total points
//            user.setTotalPoints(0);
//        }
//
//        return userChallengeRepository.save(existingUserChallenge);
//    }

    public UserChallenge completeChallenge(Long userId, Long challengeId) {
        User user = userRepository.findById(userId).orElseThrow(() -> new EntityNotFoundException(User.class.getName(), String.valueOf(userId)));
        Challenge challenge = challengeRepository.findById(challengeId).orElseThrow(() -> new EntityNotFoundException(Challenge.class.getName(), String.valueOf(challengeId)));

        UserChallenge existingUserChallenge = userChallengeRepository.findByUserAndChallenge(user, challenge);
        if (existingUserChallenge == null) {
            throw new IllegalArgumentException("Challenge is not assigned to user");
        }

        existingUserChallenge.setStatus(Status.COMPLETED);
        existingUserChallenge.setCompletionDate(LocalDate.now());

        // Add challenge points to user's total points
        user.setTotalPoints(user.getTotalPoints() + challenge.getPointValue());
        userChallengeRepository.save(existingUserChallenge);

        return existingUserChallenge;
    }

    public List<UserChallenge> getAllChallengesForUser(Long userId) {
        // This can make use of a custom query or a direct relationship from the User entity, depending on design.
        return userChallengeRepository.findByUserId(userId);
    }

    // Get a specific UserChallenge by ID
    public Optional<UserChallenge> getUserChallengeById(Long id) {
        return userChallengeRepository.findById(id);
    }

//    public UserChallenge updateUserChallenge(UserChallenge userChallenge) {
//        return userChallengeRepository.save(userChallenge);
//    }
//
//    public void deleteUserChallenge(Long id) {
//        userChallengeRepository.deleteById(id);
//    }

    // Get the status of a challenge for a specific user
    public Status getStatusForUserChallenge(Long userId, Long challengeId) {
        Optional<UserChallenge> userChallengeOptional = userChallengeRepository.findByUserIdAndChallengeId(userId, challengeId);
        return userChallengeOptional.map(UserChallenge::getStatus).orElse(null);
    }

}
