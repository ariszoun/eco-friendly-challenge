package com.aris.ecochallenge.service;

import com.aris.ecochallenge.exception.EntityNotFoundException;
import com.aris.ecochallenge.model.Prize;
import com.aris.ecochallenge.model.UserPrize;
import com.aris.ecochallenge.repository.PrizeRepository;
import com.aris.ecochallenge.repository.UserPrizeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class PrizeService {

    private final PrizeRepository prizeRepository;
    private final UserPrizeRepository userPrizeRepository;

    @Autowired
    public PrizeService(PrizeRepository prizeRepository, UserPrizeRepository userPrizeRepository) {
        this.prizeRepository = prizeRepository;
        this.userPrizeRepository = userPrizeRepository;
    }

    public Prize createPrize(Prize prize) {
        return prizeRepository.save(prize);
    }

    public List<Prize> getAllPrizes() {
        return prizeRepository.findAll();
    }

    public Prize getPrizeById(Long id) {
        return prizeRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException(Prize.class.getName(), String.valueOf(id)));
    }

    public UserPrize assignPrizeToUser(Long userId, Long prizeId) {
        // Fetch prize from the repository
        Prize prize = getPrizeById(prizeId);

        // Create a new UserPrize object
        UserPrize userPrize = new UserPrize();
        userPrize.setId(userId);
        userPrize.setPrize(prize);

        // Save to the repository
        return userPrizeRepository.save(userPrize);
    }

    public Prize updatePrize(Prize prize) {
        if (prizeRepository.existsById(prize.getId())) {
            return prizeRepository.save(prize);
        } else {
            throw new EntityNotFoundException(Prize.class.getName(), String.valueOf(prize.getId()));
        }
    }

    public void deletePrize(Long id) {
        if (!prizeRepository.existsById(id)) {
            throw new EntityNotFoundException(Prize.class.getName(), String.valueOf(id));
        }
        prizeRepository.deleteById(id);
    }

    public void deleteAllPrizes() {
        prizeRepository.deleteAll();
    }
}
