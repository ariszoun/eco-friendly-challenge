package com.aris.ecochallenge.service;

import com.aris.ecochallenge.exception.EntityNotFoundException;
import com.aris.ecochallenge.model.Prize;
import com.aris.ecochallenge.model.User;
import com.aris.ecochallenge.model.UserPrize;
import com.aris.ecochallenge.repository.PrizeRepository;
import com.aris.ecochallenge.repository.UserPrizeRepository;
import com.aris.ecochallenge.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
public class UserPrizeService {

    private final UserPrizeRepository userPrizeRepository;
    private final UserRepository userRepository;
    private final PrizeRepository prizeRepository;

    @Autowired
    public UserPrizeService(UserPrizeRepository userPrizeRepository, UserRepository userRepository,
                            PrizeRepository prizeRepository) {
        this.userPrizeRepository = userPrizeRepository;
        this.userRepository = userRepository;
        this.prizeRepository = prizeRepository;
    }

//    public UserPrize assignPrizeToUser(UserPrize userPrize) {
//        return userPrizeRepository.save(userPrize);
//    }

    public void assignPrizeIfUserIsEligible(Long userId) {
        User user = userRepository.findById(userId).orElseThrow(() -> new EntityNotFoundException(User.class.getName(), String.valueOf(userId)));

        if(user.getTotalPoints() >= 5) {
            List<Prize> prizes = prizeRepository.findPrizesByPointThresholdLessThanEqual(5);

            if (prizes.isEmpty()) {
                throw new EntityNotFoundException(Prize.class.getName(), "Point threshold: 5");
            }

            // Assign random prize to user in intersection table
            Prize prize = prizes.get(new Random().nextInt(prizes.size()));
            UserPrize userPrize = new UserPrize();
            userPrize.setUser(user);
            userPrize.setPrize(prize);
            userPrizeRepository.save(userPrize);

            // Reset user's total points
            user.setTotalPoints(0);
            userRepository.save(user);
        }
    }

    public List<UserPrize> getAllUserPrizes() {
        return userPrizeRepository.findAll();
    }

    public Optional<UserPrize> getUserPrizeById(Long id) {
        return userPrizeRepository.findById(id);
    }

//    public void deleteUserPrize(Long id) {
//        userPrizeRepository.deleteById(id);
//    }

    public void deleteUserPrize(Long id) {
        if(userPrizeRepository.existsById(id)) {
            userPrizeRepository.deleteById(id);
        } else {
            throw new EntityNotFoundException(UserPrize.class.getName(), String.valueOf(id));
        }
    }

    public void deleteAllPrizesFromUser(Long userId) {
        // First, fetch the user to ensure they exist
        User user = userRepository.findById(userId).orElseThrow(() ->
                new EntityNotFoundException(User.class.getName(), String.valueOf(userId)));

        // Find all UserPrize associations for this user
        List<UserPrize> userPrizes = userPrizeRepository.findAllByUser(user);

        // Delete all found associations
        userPrizeRepository.deleteAll(userPrizes);
    }

}
