//package com.aris.ecochallenge.service;
//
//import com.aris.ecochallenge.config.KafkaConnectConfig;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.context.event.ApplicationReadyEvent;
//import org.springframework.context.event.EventListener;
//import org.springframework.http.HttpEntity;
//import org.springframework.http.HttpHeaders;
//import org.springframework.http.MediaType;
//import org.springframework.stereotype.Service;
//import org.springframework.web.client.RestTemplate;
//
//import java.util.HashMap;
//import java.util.Map;
//
//@Service
//public class DebeziumConnectorService {
//
//    @Autowired
//    private KafkaConnectConfig kafkaConnectConfig;
//
//    @EventListener(ApplicationReadyEvent.class)
//    public void registerConnector() {
//        RestTemplate restTemplate = new RestTemplate();
//        String connectorConfigUrl = kafkaConnectConfig.getUrl() + "/connectors" +
//                "/" + kafkaConnectConfig.getConnectorName() + "/config";
//
//        String connectorConfig = "{" +
//                "  \"name\": \"eco-friendly-challenge-connector\"," +
//                "  \"config\": {" +
//                "    \"connector.class\": \"io.debezium.connector.postgresql.PostgresConnector\"," +
//                "    \"database.hostname\": \"host.docker.internal\"," +
//                "    \"database.port\": \"5432\"," +
//                "    \"database.user\": \"adminuser\"," +
//                "    \"database.password\": \"adminpass\"," +
//                "    \"database.dbname\": \"eco_friendly_challenge\"," +
//                "    \"database.server.name\": \"eco-friendly-challenge-db\"," +
//                "    \"plugin.name\": \"pgoutput\"," +
//                "    \"slot.name\": \"debezium_slot\"," +
//                "    \"publication.name\": \"debezium_publication\"," +
//                "    \"snapshot.mode\": \"initial\"," +
//                "    \"table.include.list\": \"public.outbox_event\"," +
//                "    \"key.converter\": \"org.apache.kafka.connect.json.JsonConverter\"," +
//                "    \"value.converter\": \"org.apache.kafka.connect.json.JsonConverter\"," +
//                "    \"key.converter.schemas.enable\": \"false\"," +
//                "    \"value.converter.schemas.enable\": \"false\"," +
//                "    \"topic.creation.default.replication.factor\": \"1\"," +
//                "    \"topic.creation.default.partitions\": \"1\"" +
//                "  }" +
//                "}";
//
//        HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.APPLICATION_JSON);
//        HttpEntity<String> connConfigEntity = new HttpEntity<>(connectorConfig, headers);
//
//        restTemplate.put(connectorConfigUrl, connConfigEntity);
//    }
//
//}
