package com.aris.ecochallenge.service;

import com.aris.ecochallenge.event.UserLoggedInEventPayload;
import com.aris.ecochallenge.exception.EntityAlreadyExistsException;
import com.aris.ecochallenge.exception.EntityNotFoundException;
import com.aris.ecochallenge.model.*;
import com.aris.ecochallenge.repository.OutboxEventRepository;
import com.aris.ecochallenge.repository.RoleRepository;
import com.aris.ecochallenge.repository.UserChallengeRepository;
import com.aris.ecochallenge.repository.UserRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.naming.AuthenticationException;
import java.time.LocalDateTime;
import java.util.*;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;
    private final UserChallengeRepository userChallengeRepository;
    private final OutboxEventRepository outboxEventRepository;
    private final ObjectMapper objectMapper;

    @Autowired
    public UserService(UserRepository userRepository, PasswordEncoder passwordEncoder, RoleRepository roleRepository,
                       UserChallengeRepository userChallengeRepository, OutboxEventRepository outboxEventRepository,
                       ObjectMapper objectMapper) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
        this.userChallengeRepository = userChallengeRepository;
        this.outboxEventRepository = outboxEventRepository;
        this.objectMapper = objectMapper;
    }

    public User registerUser(User user) {
        if(userRepository.findByEmail(user.getEmail()).isPresent()) {
            throw new EntityAlreadyExistsException(User.class.getName(), user.getEmail());
        }

        Role userRole = roleRepository.findByName("ROLE_ADMIN")
                .orElseThrow(() -> new EntityNotFoundException(Role.class.getName(), "ADMIN"));

        Set<Role> roles = new HashSet<>();
        roles.add(userRole);

        user.setRoles(roles);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        return userRepository.save(user);
    }

    public User validateUser(String email, String password) throws AuthenticationException {
        Optional<User> foundUser = userRepository.findByEmail(email);
        if(foundUser.isPresent() && passwordEncoder.matches(password, foundUser.get().getPassword())) {
            User user = foundUser.get();


            //Store the UserLoggedInEvent in the outbox_event table
            UserLoggedInEventPayload eventPayload =
                    new UserLoggedInEventPayload(user.getUsername(), user.getEmail(), LocalDateTime.now());
            try {
                String eventPayoladString = objectMapper.writeValueAsString(eventPayload);
                OutboxEvent event = new OutboxEvent("UserLoggedInEvent", eventPayoladString);
                outboxEventRepository.save(event);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
            return user;
        }
        throw new AuthenticationException("Invalid email or password.");
    }


    public void assignRolesToUser(Long userId, Set<Role> roles) {
        Optional<User> optionalUser = userRepository.findById(userId);
        if (optionalUser.isEmpty()) {
            throw new EntityNotFoundException(User.class.getName(), String.valueOf(userId));
        }
        User user = optionalUser.get();
        user.setRoles(roles);
        userRepository.save(user);
    }

    /**
     * Gets the total points accumulated by a user from completed challenges.
     *
     * @param userId the ID of the user.
     * @return the total points of the user.
     */
    public int getUserTotalPoints(Long userId) {
        User user = getUserById(userId).orElseThrow(() -> new EntityNotFoundException(User.class.getName(), String.valueOf(userId)));

        // Fetch all UserChallenge entities for this user where status is COMPLETED
        List<UserChallenge> completedChallenges = userChallengeRepository.findByUserAndStatus(user, Status.COMPLETED);

        // Calculate total points
        return completedChallenges.stream()
                .mapToInt(uc -> uc.getChallenge().getPointValue())
                .sum();

    }

    public Optional<User> getUserById(Long userId) {
        return userRepository.findById(userId);
    }

    public User getUserByEmail(String email) {
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException(User.class.getName(), email));
    }

    public User updateUser(User user) {
        if (userRepository.existsById(user.getId())) {
            return userRepository.save(user);
        } else {
            // User not found
            throw new RuntimeException("User not found");
        }
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public void deleteUser(Long userId) {
        userRepository.deleteById(userId);
    }
}
