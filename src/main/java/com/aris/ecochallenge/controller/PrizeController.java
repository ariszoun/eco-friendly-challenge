package com.aris.ecochallenge.controller;

import com.aris.ecochallenge.model.Prize;
import com.aris.ecochallenge.service.PrizeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/prizes")
public class PrizeController {

    private final PrizeService prizeService;

    @Autowired
    public PrizeController(PrizeService prizeService) {
        this.prizeService = prizeService;
    }

    @PostMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity<Prize> createPrize(@RequestBody Prize prize) {
        return ResponseEntity.ok(prizeService.createPrize(prize));
    }

    @GetMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity<List<Prize>> getAllPrizes() {
        return ResponseEntity.ok(prizeService.getAllPrizes());
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity<Prize> getPrizeById(@PathVariable Long id) {
        return ResponseEntity.ok(prizeService.getPrizeById(id));
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity<Prize> updatePrize(@RequestBody Prize prize, @PathVariable Long id) {
        prize.setId(id);
        return ResponseEntity.ok(prizeService.updatePrize(prize));
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity<Void> deletePrize(@PathVariable Long id) {
        prizeService.deletePrize(id);
        return ResponseEntity.noContent().build();
    }
}
