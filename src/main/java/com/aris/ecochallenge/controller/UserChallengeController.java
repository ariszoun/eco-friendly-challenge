package com.aris.ecochallenge.controller;

import com.aris.ecochallenge.model.UserChallenge;
import com.aris.ecochallenge.repository.UserChallengeRepository;
import com.aris.ecochallenge.security.UserPrincipal;
import com.aris.ecochallenge.service.UserChallengeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("${api.base-path}/${api.version}/user-challenges")
public class UserChallengeController {

    private final UserChallengeService userChallengeService;

    public UserChallengeController (UserChallengeService userChallengeService) {
        this.userChallengeService = userChallengeService;
    }

    @PostMapping("/assign/{challengeId}")
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<UserChallenge> assignChallenge(@AuthenticationPrincipal UserPrincipal currentUser, @PathVariable Long challengeId) {
        UserChallenge userChallenge = userChallengeService.assignChallengeToUser(currentUser.getId(), challengeId);
        return new ResponseEntity<>(userChallenge, HttpStatus.CREATED);
    }

    @DeleteMapping("/unassign/{challengeId}")
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<Void> unassignChallenge(@AuthenticationPrincipal UserPrincipal currentUser, @PathVariable Long challengeId) {
        userChallengeService.unassignChallengeFromUser(currentUser.getId(), challengeId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping("/complete/{challengeId}")
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity<UserChallenge> completeChallenge(@AuthenticationPrincipal UserPrincipal currentUser, @PathVariable Long challengeId) {
        UserChallenge userChallenge = userChallengeService.completeChallenge(currentUser.getId(), challengeId);
        return new ResponseEntity<>(userChallenge, HttpStatus.OK);
    }

    // Additional controller methods...
}
