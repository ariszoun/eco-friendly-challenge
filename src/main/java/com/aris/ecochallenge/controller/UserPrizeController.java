package com.aris.ecochallenge.controller;

import com.aris.ecochallenge.model.UserPrize;
import com.aris.ecochallenge.service.UserPrizeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/user-prizes")
public class UserPrizeController {

    private final UserPrizeService userPrizeService;

    @Autowired
    public UserPrizeController(UserPrizeService userPrizeService) {
        this.userPrizeService = userPrizeService;
    }

    // This endpoint allows users to claim a prize.
//    @PostMapping
//    @PreAuthorize("hasRole('ROLE_USER')")
//    public ResponseEntity<UserPrize> assignPrizeToUser(@RequestBody UserPrize userPrize) {
//        return ResponseEntity.ok(userPrizeService.assignPrizeToUser(userPrize));
//    }
    @PostMapping("/{userId}/assignPrize")
    public ResponseEntity<Void> assignPrizeIfEligible(@PathVariable Long userId) {
        userPrizeService.assignPrizeIfUserIsEligible(userId);
        return ResponseEntity.ok().build();
    }

    @GetMapping
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity<List<UserPrize>> getAllUserPrizes() {
        return ResponseEntity.ok(userPrizeService.getAllUserPrizes());
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity<Void> deleteUserPrize(@PathVariable Long id) {
        userPrizeService.deleteUserPrize(id);
        return ResponseEntity.noContent().build();
    }
}
