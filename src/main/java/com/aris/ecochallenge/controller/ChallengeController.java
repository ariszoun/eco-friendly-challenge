package com.aris.ecochallenge.controller;

import com.aris.ecochallenge.model.Challenge;
import com.aris.ecochallenge.repository.ChallengeRepository;
import com.aris.ecochallenge.service.ChallengeService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("${api.base-path}/${api.version}/challenges")
public class ChallengeController {

    private final ChallengeService challengeService;

    @Autowired
    public ChallengeController(ChallengeService challengeService) {
        this.challengeService = challengeService;
    }

    // Admin endpoint to add a new challenge
    @PostMapping
//    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Challenge> createChallenge(@Valid @RequestBody Challenge challenge) {
        Challenge createdChallenge = challengeService.createChallenge(challenge);
        return new ResponseEntity<>(createdChallenge, HttpStatus.CREATED);
    }

    // Get all challenges
    @GetMapping
    public List<Challenge> getAllChallenges() {
        return challengeService.listAllChallenges();
    }

    // Get a challenge by its ID
    @GetMapping("/{challengeId}")
    public Challenge getChallengeById(@PathVariable Long challengeId) {
        return challengeService.getChallengeById(challengeId);
    }

    // Admin endpoint to update a challenge
    @PutMapping("/{challengeId}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public Challenge updateChallenge(@PathVariable Long challengeId, @Valid @RequestBody Challenge challenge) {
        // Ensure the ID provided in the path matches the ID in the request body
        if (!challengeId.equals(challenge.getId())) {
            throw new IllegalArgumentException("Mismatched challenge ID in path and request body");
        }
        return challengeService.updateChallenge(challenge);
    }

    // Admin endpoint to delete a challenge
    @DeleteMapping("/{challengeId}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity<Void> deleteChallenge(@PathVariable Long challengeId) {
        challengeService.deleteChallenge(challengeId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
