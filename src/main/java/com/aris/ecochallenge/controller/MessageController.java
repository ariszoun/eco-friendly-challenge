//package com.aris.ecochallenge.controller;
//
//import com.aris.ecochallenge.service.KafkaConsumerService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.util.List;
//
//@RestController
//@RequestMapping("${api.base-path}/${api.version}/messages")
//public class MessageController {
//
//    private final KafkaConsumerService kafkaConsumerService;
//
//    @Autowired
//    public MessageController(KafkaConsumerService kafkaConsumerService) {
//        this.kafkaConsumerService = kafkaConsumerService;
//    }
//
//    @GetMapping
//    public ResponseEntity<List<String>> getMessages() {
//        return ResponseEntity.ok(kafkaConsumerService.getMessages());
//    }
//
//}
