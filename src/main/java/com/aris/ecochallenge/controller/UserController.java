package com.aris.ecochallenge.controller;

import com.aris.ecochallenge.dto.LoginRequest;
import com.aris.ecochallenge.exception.EntityNotFoundException;
import com.aris.ecochallenge.model.Role;
import com.aris.ecochallenge.model.User;
import com.aris.ecochallenge.security.UserPrincipal;
import com.aris.ecochallenge.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import jakarta.validation.Valid;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.naming.AuthenticationException;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("${api.base-path}/${api.version}/users")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/{userId}")
    public ResponseEntity<User> getUserById(@PathVariable Long userId, @AuthenticationPrincipal UserPrincipal currentUser) {
        if (!currentUser.getId().equals(userId) && !currentUser.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMIN"))) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        User user = userService.getUserById(userId)
                .orElseThrow(() -> new EntityNotFoundException(User.class.getName(), String.valueOf(userId)));
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @GetMapping("/email")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity<User> getUserByEmail(@RequestParam("email") String email) {
//        if (!currentUser.getUsername().equals(email)) {
//            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
//        }
//        User user = userService.getUserByEmail(email);
//        return new ResponseEntity<>(user, HttpStatus.OK);
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        String currentUsername;
        if (principal instanceof UserDetails) {
            currentUsername = ((UserDetails) principal).getUsername();
        } else {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        if (!email.equals(currentUsername)) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }

        User user = userService.getUserByEmail(email);
        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<List<User>> getAllUsers() {
        List<User> users = userService.getAllUsers();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<User> updateUser(@Valid @RequestBody User user, @AuthenticationPrincipal UserPrincipal currentUser) {
        if (!currentUser.getId().equals(user.getId()) && !currentUser.getAuthorities().contains(new SimpleGrantedAuthority("ROLE_ADMIN"))) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        User updatedUser = userService.updateUser(user);
        return new ResponseEntity<>(updatedUser, HttpStatus.OK);
    }

    @DeleteMapping("/{userId}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity<Void> deleteUser(@PathVariable Long userId) {
        userService.deleteUser(userId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping("/{userId}/roles")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN', 'ROLE_USER')")
    public ResponseEntity<Void> assignRolesToUser(@PathVariable Long userId, @RequestBody Set<Role> roles) {
        userService.assignRolesToUser(userId, roles);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
