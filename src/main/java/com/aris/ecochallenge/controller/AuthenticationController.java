package com.aris.ecochallenge.controller;

import com.aris.ecochallenge.dto.LoginRequest;
import com.aris.ecochallenge.dto.LoginResponse;
import com.aris.ecochallenge.event.UserLoggedOutEventPayload;
import com.aris.ecochallenge.model.OutboxEvent;
import com.aris.ecochallenge.model.User;
import com.aris.ecochallenge.repository.OutboxEventRepository;
import com.aris.ecochallenge.repository.UserRepository;
import com.aris.ecochallenge.security.UserAuthDetailsService;
import com.aris.ecochallenge.security.jwt.JwtUtil;
import com.aris.ecochallenge.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.naming.AuthenticationException;
import java.time.LocalDateTime;
import java.util.Optional;

@RestController
@RequestMapping("${api.base-path}/${api.version}/auth")
public class AuthenticationController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtil jwtUtil;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private UserAuthDetailsService userAuthDetailsService;

    @Autowired
    private OutboxEventRepository outboxEventRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @PostMapping("/login")
    public ResponseEntity<LoginResponse> createAuthenticationToken(@RequestBody LoginRequest loginRequest) {
        try {
            User user = userService.validateUser(loginRequest.getEmail(), loginRequest.getPassword());
            final UserDetails userDetails = userAuthDetailsService.loadUserByUsername(user.getEmail());
            final String jwt = jwtUtil.generateToken(userDetails.getUsername());
            return ResponseEntity.ok(new LoginResponse(jwt));

        } catch (AuthenticationException e) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new LoginResponse(null, "Incorrect email or password"));
        }
    }

    @PostMapping("/register")
    public ResponseEntity<User> registerUser(@Valid @RequestBody User user) {
        User registeredUser = userService.registerUser(user);
        return new ResponseEntity<>(registeredUser, HttpStatus.CREATED);
    }

    @PostMapping("/logout")
    public ResponseEntity<String> logout(@RequestHeader("Authorization") String authHeader) {
        if (authHeader != null && authHeader.startsWith("Bearer ")) {
            String token = authHeader.substring(7); // Extract the token
            String userEmail = extractEmailFromToken(token);

            if (userEmail != null) {
                Optional<User> foundUser = userRepository.findByEmail(userEmail);
                if (foundUser.isPresent()) {
                    User user = foundUser.get();

                    // Store the UserLoggedOutEvent in the outbox_event table
                    UserLoggedOutEventPayload eventPayload = new UserLoggedOutEventPayload(user.getUsername(), user.getEmail(), LocalDateTime.now());
                    try {
                        String eventPayloadString = objectMapper.writeValueAsString(eventPayload);
                        OutboxEvent event = new OutboxEvent("UserLoggedOutEvent", eventPayloadString);
                        outboxEventRepository.save(event);
                    } catch (JsonProcessingException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }
        // For this approach, the server doesn't do much except the event store.
        // The client should discard the JWT and ensure it's not sent with future requests.
        // Token is considered invalid/expired, the client application clears the token from its storage (local or session).
        // Then the client detects a 401 Unauthorized response so:
        // 1) Displaying a message to the user that is logged out.
        // 2) Redirecting the user to the welcome/home page.
        return ResponseEntity.ok("Logged out successfully");
    }

    private String extractEmailFromToken(String token) {
        try {
            return jwtUtil.extractEmail(token);
        } catch (Exception e) {
            return e.getMessage();
        }
    }
}
