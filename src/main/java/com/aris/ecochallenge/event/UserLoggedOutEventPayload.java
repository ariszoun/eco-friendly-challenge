package com.aris.ecochallenge.event;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.time.LocalDateTime;

public class UserLoggedOutEventPayload {

    private String username;
    private String email;
    private LocalDateTime logoutTime;

    public UserLoggedOutEventPayload() {}

    public UserLoggedOutEventPayload(String username, String email, LocalDateTime logoutTime) {
        this.username = username;
        this.email = email;
        this.logoutTime = logoutTime;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDateTime getLogoutTime() {
        return logoutTime;
    }

    public void setLogoutTime(LocalDateTime logoutTime) {
        this.logoutTime = logoutTime;
    }

    public String toJSON() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(this);
    }
}
