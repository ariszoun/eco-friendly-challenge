package com.aris.ecochallenge.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


/**
 * Swagger Configuration.
 * <p>
 * This configuration enables Swagger for documenting the API. Swagger provides
 * an interactive UI to explore and test the API endpoints. It generates a comprehensive
 * documentation based on the code and annotations.
 * </p>
 * <p>
 * To access the Swagger UI, navigate to:
 * <a href="http://localhost:8080/swagger-ui/">http://localhost:8080/swagger-ui/</a>
 * </p>
 * <p>
 * Ensure that in production, the Swagger UI is secured and not exposed publicly unless intended.
 * </p>
 *
 * @author Zounarakis Aris
 * @version 1.0
 * @since 2023-10-08
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build();
    }
}
