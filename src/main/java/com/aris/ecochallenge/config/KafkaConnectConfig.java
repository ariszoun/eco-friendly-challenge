//package com.aris.ecochallenge.config;
//
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration
//@ConfigurationProperties(prefix = "kafka.connect")
//public class KafkaConnectConfig {
//
//    private String url;
//    private String connectorName;
//
//    public String getUrl() {
//        return url;
//    }
//
//    public void setUrl(String url) {
//        this.url = url;
//    }
//
//    public String getConnectorName() {
//        return connectorName;
//    }
//
//    public void setConnectorName(String connectorName) {
//        this.connectorName = connectorName;
//    }
//}
