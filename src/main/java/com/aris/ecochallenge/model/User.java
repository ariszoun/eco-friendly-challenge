package com.aris.ecochallenge.model;


import javax.persistence.*;

import java.util.*;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String username;

    @Column(nullable = false, unique = true)
    private String password;

    @Column(nullable = false, unique = true)
    private String email;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles = new HashSet<>();

    @Column(name = "total_points", nullable = false)
    private Integer totalPoints = 0;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<UserChallenge> userChallenges = new ArrayList<>();

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<UserPrize> userPrizes = new ArrayList<>();

    public User() {}

    public User(String username, String password, String email) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.totalPoints = 0;
    }

    public User(String username, String password, String email, Integer totalPoints) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.totalPoints = totalPoints;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(Integer totalPoints) {
        this.totalPoints = totalPoints;
    }

    public List<UserChallenge> getUserChallenges() {
        return userChallenges;
    }

    public void setUserChallenges(List<UserChallenge> userChallenges) {
        this.userChallenges = userChallenges;
    }

    public List<UserPrize> getUserPrizes() {
        return userPrizes;
    }

    public void setUserPrizes(List<UserPrize> userPrizes) {
        this.userPrizes = userPrizes;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    /**
     * Utility methods: addRole, removeRole, clearRoles.
     * Both sides of the relationship are managed for data consistency.
     * This affects only java memory and not db.
     * To persist changes use repositories.
     * */
    public void addRole(Role role) {
        roles.add(role);
        role.getUsers().add(this);
    }

    public void removeRole(Role role) {
        roles.remove(role);
        role.getUsers().remove(this);
    }

    public void clearRoles() {
        for (Role role : roles) {
            role.getUsers().remove(this);
        }
        roles.clear();
    }

    // Method to add UserChallenge for bidirectional relationship management
    public void addUserChallenge(UserChallenge userChallenge) {
        userChallenges.add(userChallenge);
        userChallenge.setUser(this);
    }

    // Method to add UserPrize for bidirectional relationship management
    public void addUserPrize(UserPrize userPrize) {
        userPrizes.add(userPrize);
        userPrize.setUser(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
