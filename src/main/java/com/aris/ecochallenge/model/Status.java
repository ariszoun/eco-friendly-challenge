package com.aris.ecochallenge.model;

public enum Status {

    IN_PROGRESS,
    COMPLETED,
    NOT_STARTED,
    RETAKE

}
