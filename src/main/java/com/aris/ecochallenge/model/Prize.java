package com.aris.ecochallenge.model;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "prizes")
public class Prize {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String title;

    @Column(nullable = false)
    private String description;

    /**
     * The point threshold required for a user to become eligible to claim this prize.
     * <p>
     * A user can claim this prize if their accumulated points are greater than or
     * equal to this threshold. It serves as a criterion for determining a user's
     * eligibility for the prize. For instance, if the pointThreshold is set to 150,
     * a user with 150 or more points can claim this prize.
     * </p>
     */
    @Column(name = "point_threshold", nullable = false)
    private int pointThreshold;

    @OneToMany(mappedBy = "prize", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<UserPrize> userPrizes = new ArrayList<>();

    public Prize(){}

    public Prize(String title, String description, Integer pointThreshold) {
        this.title = title;
        this.description = description;
        this.pointThreshold = pointThreshold;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPointThreshold() {
        return pointThreshold;
    }

    public void setPointThreshold(int pointThreshold) {
        this.pointThreshold = pointThreshold;
    }

    public List<UserPrize> getUserPrizes() {
        return userPrizes;
    }

    public void setUserPrizes(List<UserPrize> userPrizes) {
        this.userPrizes = userPrizes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Prize prize = (Prize) o;
        return Objects.equals(id, prize.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
