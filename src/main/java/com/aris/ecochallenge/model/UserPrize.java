package com.aris.ecochallenge.model;

import javax.persistence.*;

import java.time.LocalDate;
import java.util.Objects;

@Entity
@Table(name = "user_prizes")
public class UserPrize {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne
    @JoinColumn(name = "prize_id")
    private Prize prize;

    @Column(name = "claim_date", nullable = false)
    private LocalDate claimDate = LocalDate.now();

    public UserPrize(){}

    public UserPrize(User user, Prize prize) {
        this.user = user;
        this.prize = prize;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Prize getPrize() {
        return prize;
    }

    public void setPrize(Prize prize) {
        this.prize = prize;
    }

    public LocalDate getClaimDate() {
        return claimDate;
    }

    public void setClaimDate(LocalDate claimDate) {
        this.claimDate = claimDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserPrize userPrize = (UserPrize) o;
        return Objects.equals(id, userPrize.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
