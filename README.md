# Eco-Friendly Challenge App

## About

The Eco Challenge App encourages users to participate in eco-friendly challenges, collect points, and win prizes. It aims to promote sustainable practices by rewarding users for their eco-friendly actions.
Features. 

* User Registration and Login 
* Profile Management - Needs work (frontend mostly)
* Eco-friendly Challenges - Needs work (frontend mostly)
* Prize Management - Needs work (frontend mostly)
* Admin capabilities for challenge and prize management - Needs work (both back and front end)
* Notifications for challenge completions and prize acquisitions - Needs work (frontend mostly)

### Technology Stack

* Backend: Java, Spring Boot, Spring Security, JWT for Authentication
* Database: PostgreSQL, Flyway
* Frontend: React
* Messaging and event handling: Kafka
* Testing: JUnit 5, Mockito

## Setting up the App

### Clone the Repository:
* ``git clone: REPOSITORY_URL``

### Navigate to the directory:
* ``cd REPOSITORY_NAME``

### Set up the Database:

* Ensure you have PostgreSQL installed.
* Set up a database with the name ``eco_challenge_db`` (or your preferred name).
* Update ``src/main/resources/application.properties`` with your database credentials.
#### Flyway Migration:
* The app uses Flyway for database migrations. 
Upon starting the application, Flyway will automatically set up the necessary tables and relationships based on the 
migration scripts provided in the ``src/main/resources/db/migration directory``.

### Build and Run the App:
* ``./mvnw spring-boot:run``

The backend server will start and is accessible at http://localhost:8080/.

## Reference Documentation

For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/3.1.4/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/3.1.4/maven-plugin/reference/html/#build-image)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/3.1.4/reference/htmlsingle/index.html#data.sql.jpa-and-spring-data)
* [Spring Web](https://docs.spring.io/spring-boot/docs/3.1.4/reference/htmlsingle/index.html#web)
* [Spring Security](https://docs.spring.io/spring-boot/docs/3.1.4/reference/htmlsingle/index.html#web.security)
* [Spring for Apache Kafka](https://docs.spring.io/spring-boot/docs/3.1.4/reference/htmlsingle/index.html#messaging.kafka)

## Guides

The following guides illustrate how to use some features concretely:

* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)
* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/rest/)
* [Securing a Web Application](https://spring.io/guides/gs/securing-web/)
* [Spring Boot and OAuth2](https://spring.io/guides/tutorials/spring-boot-oauth2/)
* [Authenticating a User with LDAP](https://spring.io/guides/gs/authenticating-ldap/)

